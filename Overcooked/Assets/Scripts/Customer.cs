using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Customer : MonoBehaviour
{
    [SerializeField] List<DishesSO> dishes;

    [SerializeField] public List<GameObject> dishes_UI;

    public DishesSO requestedDish;

    public int randomDish;
    private void Start()
    {
        randomDish = Random.Range(0, dishes.Count);

        requestedDish = dishes[randomDish];
    }
    //Ingredienti
    //1 - Pane
    //2 - Pollo
    //3 - Insalata


    //piatto 1
    //Chicken Borgir = Pane + Pollo
    //Sandwich = Pane + Insalata
    //Caesar Salad = Insalata + Pollo
}
