using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class CustomerSpawner : MonoBehaviour
{
    [HideInInspector] public List<GameObject> Customers = new List<GameObject>();
    [HideInInspector] public List<GameObject> SpawnedCustomers = new List<GameObject>();

    public List<Transform> waypoints;

    public List<GameObject> CustomerPrefab = new List<GameObject>();

    int timeToSpawn = 0;

    int prefabToSpawn;

    GameManager gm;
    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();

        //initialize
        Instantiate(CustomerPrefab[prefabToSpawn], transform.position, transform.rotation);
        prefabToSpawn++;
        timeToSpawn = Random.Range(7, 10);

        StartCoroutine(SpawnCountdown(timeToSpawn));
    }
    IEnumerator SpawnCountdown(int time)
    {
        while(true)
        {
            yield return new WaitForSeconds(time);

            if (gm.isDayTime && waypoints.Count > 0)
            {
                timeToSpawn = Random.Range(10, 15);
                Instantiate(CustomerPrefab[prefabToSpawn], transform.position, transform.rotation);
                if(prefabToSpawn == CustomerPrefab.Count - 1)
                    prefabToSpawn= 0;
                else
                    prefabToSpawn++;
            }
        }
    }
}
