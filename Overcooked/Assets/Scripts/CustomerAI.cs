using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CustomerAI : MonoBehaviour
{
    [SerializeField] Transform UIPosition;

    CustomerSpawner spawner;

    public bool questCompleted;
    public bool questPending;

    NavMeshAgent agent;

    Animator anim;
    Customer customer;

    GameObject Order_UI;

    Camera cam;

    Transform destination;

    private void Start()
    {
        cam = Camera.main;
        customer = GetComponent<Customer>();
        anim = GetComponent<Animator>();
        spawner = FindObjectOfType<CustomerSpawner>();
        agent = GetComponent<NavMeshAgent>();

        questCompleted = false;
        questPending= false;

        anim.SetBool("IsWalking", true);

        destination = spawner.waypoints[Random.Range(0, spawner.waypoints.Count)];

        agent.SetDestination(destination.position);

        spawner.waypoints.Remove(destination);

        spawner.SpawnedCustomers.Add(gameObject);


        InstantiateOrderUI();
    }
    private void Update()
    {
        if (questCompleted)
        {
            agent.SetDestination(spawner.transform.position);
            anim.SetBool("IsWalking", true);
            Order_UI.SetActive(false);
        }

        if (agent.remainingDistance < 0.05f && !questCompleted && !questPending)
        {
            anim.SetBool("IsWalking", false);
            spawner.Customers.Add(gameObject);
            questPending = true;

            Order_UI.SetActive(true);
            Order_UI.transform.rotation = Quaternion.LookRotation(-cam.transform.position);
            Order_UI.transform.position = UIPosition.position;
        }
        if (Vector3.Distance(spawner.transform.position, transform.position) < 0.5f && questCompleted)
        {
            spawner.waypoints.Add(destination);
            Destroy(gameObject);
        }
    }
    void InstantiateOrderUI()
    {
        Canvas worldCanvas = GameObject.Find("WorldCanvas").GetComponent<Canvas>();

        switch (customer.randomDish)
        {
            case 0:
                Order_UI = Instantiate(customer.dishes_UI[0], UIPosition.position, Quaternion.identity, worldCanvas.transform);
                break;

            case 1:
                Order_UI = Instantiate(customer.dishes_UI[1], UIPosition.position, Quaternion.identity, worldCanvas.transform);
                break;

            case 2:
                Order_UI = Instantiate(customer.dishes_UI[2], UIPosition.position, Quaternion.identity, worldCanvas.transform);
                break;

            default:
                break;
        }
    }
}
