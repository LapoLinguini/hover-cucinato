using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public float DayTime;

    public bool isDayTime;

    [SerializeField] Light dirLight;

    private void Start()
    {
        DayTime = 0;
        isDayTime = true;
    }
    private void Update()
    {
        Clock();
        TimeSpeed();
    }
    void Clock()
    {
        if (DayTime < 60)
        {
            isDayTime = true;
            DayTime += Time.deltaTime;
        }
        else
            isDayTime = false;

        dirLight.intensity = Mathf.Lerp(1.25f, 0.25f, DayTime / 60);
    }
    void TimeSpeed()
    {

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Time.timeScale = 1;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Time.timeScale = 2;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            Time.timeScale = 4;
        }
    }
}
