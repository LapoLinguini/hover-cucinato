using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.AI;

public class BTConttroller : MonoBehaviour
{
    [Header("Points Of Interest: ")]
    [SerializeField] Transform AssembleP;
    [SerializeField] Transform SellP;
    [SerializeField] Transform BreadRP;
    [SerializeField] Transform ChickenRP;
    [SerializeField] Transform SaladRP;
    [SerializeField] Transform BedP;

    //[Header("Resources: ")]
    //[SerializeField] List<int> Resources = new List<int>();

    [SerializeField] int maxBread;
    [SerializeField] int maxChicken;
    [SerializeField] int maxSalad;
    public int currentBread;
    public int currentChicken;
    public int currentSalad;
    bool refilling;

    [Header("UI: ")]
    [SerializeField] TextMeshProUGUI currentBreadTXT;
    [SerializeField] TextMeshProUGUI currentChickenTXT;
    [SerializeField] TextMeshProUGUI currentSaladTXT;

    [Header("Particles: ")]
    [SerializeField] GameObject AssembleParticle;
    [SerializeField] Transform ParticlesPos;
    bool particles;

    [Header("RotTransforms: ")]
    [SerializeField] Transform BreadM;
    [SerializeField] Transform ChickenM;
    [SerializeField] Transform SaladM;
    [SerializeField] Transform AssembleM;

    BTNode.Status treeStatus = BTNode.Status.Running;

    //references
    BTRoot tree;
    NavMeshAgent agent;
    CustomerSpawner customerSpawner;
    Customer customer;
    GameManager gm;

    Animator anim;

    //priv
    Vector3 goToCustomer;
    public bool isServingCust;

    public bool stopCor;
    private void Start()
    {
        anim = GetComponent<Animator>();
        gm = FindObjectOfType<GameManager>();
        agent = GetComponent<NavMeshAgent>();
        customerSpawner = FindObjectOfType<CustomerSpawner>();

        //max resources at start
        currentBread = maxBread;
        currentChicken = maxChicken;
        currentSalad = maxSalad;

        tree = new BTRoot();

        Sequence playerAction = new Sequence("PlayerAction");
        Leaf isDayOrNot = new Leaf("Check if is day or night", CheckDay);
        Leaf goToCustomer = new Leaf("Go to customer", CheckCustomer);
        Leaf checkResources = new Leaf("Check resources based on the current quest", RefillResources);
        Leaf assembleBorgir = new Leaf("Assemble borgir", AssembleBorgir);
        Leaf delieverToCustomer = new Leaf("Deliever the borgir to the customer", DelieverToCustomer);

        playerAction.AddChild(isDayOrNot);
        playerAction.AddChild(goToCustomer);
        playerAction.AddChild(checkResources);
        playerAction.AddChild(assembleBorgir);
        playerAction.AddChild(delieverToCustomer);

        tree.AddChild(playerAction);

        tree.PrintTree();
    }

    private void Update()
    {
        treeStatus = tree.Process();

        UpdateUI();
        CheckSpawnedCustomers();
    }
    void UpdateUI()
    {
        currentBreadTXT.text = /*"Bread: " + */currentBread.ToString();
        currentChickenTXT.text = /*"Chicken: " + */currentChicken.ToString();
        currentSaladTXT.text = /*"Salad: " + */currentSalad.ToString();
    }
    void SetDestination(Vector3 destination)
    {
        agent.SetDestination(destination);
        anim.SetBool("IsRunning", true);
    }
    void CheckSpawnedCustomers()
    {
        if (customerSpawner.SpawnedCustomers.Count > 0)
            isServingCust = true;
        else
            isServingCust = false;
    }
    void RotatePlayer(Transform target)
    {
        Quaternion endRot = Quaternion.LookRotation(target.position - transform.position);
        Quaternion endRot_y = Quaternion.Euler(0, endRot.eulerAngles.y, 0);

        transform.rotation = Quaternion.Slerp(transform.rotation, endRot_y, Time.deltaTime * 5);
    }
    public BTNode.Status CheckDay()
    {
        if (!gm.isDayTime && !isServingCust)
        {
            SetDestination(BedP.transform.position);

            if (Vector3.Distance(BedP.transform.position, transform.position) < 0.5f)
            {
                gm.DayTime = 0;
                anim.SetBool("IsRunning", false);
                return BTNode.Status.Success;
            }
            return BTNode.Status.Failure;
        }
        return BTNode.Status.Success;
    }
    public BTNode.Status CheckCustomer()
    {
        if (customerSpawner.Customers.Count > 0)
        {
            goToCustomer = new Vector3(customerSpawner.Customers[0].transform.position.x, SellP.position.y, SellP.position.z);
            SetDestination(goToCustomer);

            if (Vector3.Distance(goToCustomer, transform.position) < 0.5f)
            {
                RotatePlayer(customerSpawner.Customers[0].transform);
                customer = customerSpawner.Customers[0].GetComponent<Customer>();
                anim.SetBool("IsRunning", false);
                if (!anim.GetBool("IsNodding"))
                    return BTNode.Status.Success;
            }
            else
                anim.SetBool("IsNodding", true);
        }
        return BTNode.Status.Failure;
    }
    public IEnumerator CheckResources(int currentCurrency, int requestedCurrency, int maxCurrency, Transform destination)
    {
        if (currentCurrency - requestedCurrency < 0 && !refilling)
        {
            SetDestination(destination.transform.position);
            refilling = true;
            stopCor = false;
        }
        //ROTATE
        if (destination == BreadRP)
            RotatePlayer(BreadM);
        if (destination == ChickenRP)
            RotatePlayer(ChickenM);
        if (destination == SaladRP)
            RotatePlayer(SaladM);
        //
        if (Vector3.Distance(destination.position, transform.position) < 0.5f && !stopCor)
        {

            stopCor = true;
            anim.SetBool("IsPickingUp", true);

            yield return new WaitForSeconds(3);

            anim.SetBool("IsPickingUp", false);

            if (destination == BreadRP)
                currentBread = maxCurrency;
            if (destination == ChickenRP)
                currentChicken = maxCurrency;
            if (destination == SaladRP)
                currentSalad = maxCurrency;

            refilling = false;
        }
    }
    public BTNode.Status RefillResources()
    {
        if (currentBread - customer.requestedDish.Bread < 0)
            StartCoroutine(CheckResources(currentBread, customer.requestedDish.Bread, maxBread, BreadRP));

        if (currentBread - customer.requestedDish.Bread >= 0 && currentChicken - customer.requestedDish.Chicken < 0)
        {
            StartCoroutine(CheckResources(currentChicken, customer.requestedDish.Chicken, maxChicken, ChickenRP));
        }
        if (currentBread - customer.requestedDish.Bread >= 0 && currentChicken - customer.requestedDish.Chicken >= 0)
        {
            StartCoroutine(CheckResources(currentSalad, customer.requestedDish.Salad, maxSalad, SaladRP));
        }

        if (currentBread - customer.requestedDish.Bread >= 0 && currentChicken - customer.requestedDish.Chicken >= 0 && currentSalad - customer.requestedDish.Salad >= 0)
            return BTNode.Status.Success;
        else
            return BTNode.Status.Failure;
    }
    public BTNode.Status AssembleBorgir()
    {
        SetDestination(AssembleP.transform.position);
        if (Vector3.Distance(AssembleP.transform.position, transform.position) < 0.5f)
        {
            RotatePlayer(AssembleM);

            if (!particles)
            {
                particles = true;
                Instantiate(AssembleParticle, ParticlesPos.position, Quaternion.identity);
            }

            anim.SetBool("IsRunning", false);

            if (!anim.GetBool("IsAssembling"))
            {
                currentBread -= customer.requestedDish.Bread;
                currentChicken -= customer.requestedDish.Chicken;
                currentSalad -= customer.requestedDish.Salad;
                particles = false;
                return BTNode.Status.Success;
            }
        }
        else
        {
            anim.SetBool("IsAssembling", true);
            return BTNode.Status.Failure;
        }
        return BTNode.Status.Running;
    }
    public BTNode.Status DelieverToCustomer()
    {
        SetDestination(goToCustomer);
        if (Vector3.Distance(goToCustomer, transform.position) < 0.5f)
        {
            CustomerAI customerAI = customer.GetComponent<CustomerAI>();
            customerAI.questCompleted = true;
            customerSpawner.Customers.Remove(customerSpawner.Customers[0]);
            customerSpawner.SpawnedCustomers.Remove(customerSpawner.SpawnedCustomers[0]);
            anim.SetBool("IsRunning", false);
            return BTNode.Status.Success;
        }
        return BTNode.Status.Failure;
    }
    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.red;
    //    if (agent != null)
    //        Gizmos.DrawSphere(agent.destination, 1);
    //}
    public void NodAnimationEvent()
    {
        anim.SetBool("IsNodding", false);
    }
    public void AssembleAnimationEvent()
    {
        anim.SetBool("IsAssembling", false);
    }
}

