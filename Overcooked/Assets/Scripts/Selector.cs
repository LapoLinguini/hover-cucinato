using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : BTNode
{
    public Selector(string n)
    {
        name = n;
    }

    public override Status Process()
    {
        Status childStatus = children[currentChild].Process();
        //running
        if (childStatus == Status.Running) return Status.Running;

        //success
        if (childStatus == Status.Success)
        {
            currentChild = 0;
            return Status.Success;
        } 

        //failure
        currentChild++;
        if (currentChild >= children.Count)
        {
            currentChild = 0;
            return Status.Failure;
        }

        return Status.Running;
    }
}
