using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Dish", menuName = "ScriptableObjects/Dishes")]
public class DishesSO : ScriptableObject
{
    public int Bread;
    public int Chicken;
    public int Salad;
}
